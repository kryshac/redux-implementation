import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../store';
import { REMOVE_ALL_TODOS } from '../actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @select((t: IAppState) => t.todos.length) todosLenght;
  @select((t: IAppState) => t.dataUpdate) lastUpdate;

  constructor(private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() { }

  removeAllTodos() {
    this.ngRedux.dispatch({
      type: REMOVE_ALL_TODOS
    });
  }
}

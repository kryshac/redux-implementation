import { tassign } from 'tassign';
import { ADD_TODO, REMOVE_TODO, REMOVE_ALL_TODOS } from './actions';

export interface IAppState {
  todos: any[];
  dataUpdate: Date;
}

export const INITIAL_STATE: IAppState = {
  todos: [],
  dataUpdate: null
};

function addTodo(state, action) {
  const newTodo = state.todos.concat(action.todo);
  return tassign(state, {
    todos: newTodo,
    dataUpdate: new Date()
  });
}

function removeTodo(state, action) {
  const delTodo = state.todos.filter((value, id) => id !== action.id);
  return tassign(state, {
    todos: delTodo,
    dataUpdate: new Date()
  });
}

function removeAllTodos(state, action) {
  return tassign(state, {
    todos: [],
    dataUpdate: new Date()
  });
}

export function rootReducer(state: IAppState, action: any): IAppState {
  switch (action.type) {
    case ADD_TODO:
      return addTodo(state, action);
    case REMOVE_TODO:
      return removeTodo(state, action);
    case REMOVE_ALL_TODOS:
      return removeAllTodos(state, action);
  }
  return state;
}

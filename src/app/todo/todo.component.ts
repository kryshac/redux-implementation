import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from './../store';
import { ADD_TODO, REMOVE_TODO } from './../actions';
import { Input } from '@angular/compiler/src/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  title = 'Todos';

  @select((t: IAppState) => t.todos) todos;
  // @select((s: IAppState) => s.messaging.newMessages) newMessages;

  constructor(private ngRedux: NgRedux<IAppState>) {}

  ngOnInit() {}

  addTodo(todoTitle: any) {
    const newTodo = {title: todoTitle.value};
    this.ngRedux.dispatch({
      type: ADD_TODO,
      todo: newTodo
    });
    event.preventDefault();
  }

  removeTodo(id: number) {
    this.ngRedux.dispatch({
      type: REMOVE_TODO,
      id: id
    });
  }
}
